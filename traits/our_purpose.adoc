*we*
facilitate the harmonious, efficient, principled relations, interactions, and operations of our collective

help our collective reason, communicate effectively in pursuance of our end, following our shared doctrine (standards, behaviors, rules, practices)

help each of us share things with with our collective naturally through our commons

include::{rel_traits_dir}{humanespace_purpose}[]
